package com.epam.rd.java.basic.task5;

import com.epam.rd.java.basic.task1.Flower;
import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task1.Parameter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;

public class DOMCreateXML {
    public void createXML(Flowers flowers, String outputXmlFile) throws ParserConfigurationException, IOException,
            TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        Element root = document.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        Element element;
        for (Flower f : flowers.getList()) {
            Element flower = document.createElement("flower");
            createElement(document, flower, "name", f.getName());
            createElement(document, flower, "soil", f.getSoil().getName());
            createElement(document, flower, "origin", f.getOrigin());

            createVisualParameters(document, f, flower);
            createGrowingTips(document, f, flower);

            createElement(document, flower, "multiplying", f.getMultiplying().getName());
            root.appendChild(flower);
        }
        document.appendChild(root);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new FileWriter(outputXmlFile));
        transformer.transform(source,result);
    }

    private void createElement(Document document, Element parent, String tag, String value) {
        Element element;
        element = document.createElement(tag);
        element.setTextContent(value);
        parent.appendChild(element);
    }

    private void createGrowingTips(Document document, Flower f, Element flower) {
        Element element;
        Element growingTips = document.createElement("growingTips");
        createElementWithAttribute(document, growingTips, "tempreture",
                f.getGt().getTempreture());
        element= document.createElement("lighting");
        element.setAttribute("lightRequiring", f.getGt().isLightRequiring() ? "yes" : "no");
        growingTips.appendChild(element);
        createElementWithAttribute(document, growingTips, "watering", f.getGt().getWatering());
        flower.appendChild(growingTips);
    }

    private void createVisualParameters(Document document, Flower f, Element flower) {
        Element visualParameters = document.createElement("visualParameters");
        createElement(document, visualParameters, "stemColour", f.getVp().getStemColour());
        createElement(document, visualParameters, "leafColour", f.getVp().getLeafColour());
        createElementWithAttribute(document, visualParameters, "aveLenFlower",
                f.getVp().getAveLenFlower());
        flower.appendChild(visualParameters);
    }

    private void createElementWithAttribute(Document document, Element parent, String tag, Parameter objValue) {
        Element element;
        element = document.createElement(tag);
        element.setAttribute("measure", objValue.getMeasure());
        element.setTextContent(String.valueOf(objValue.getValue()));
        parent.appendChild(element);
    }
}
