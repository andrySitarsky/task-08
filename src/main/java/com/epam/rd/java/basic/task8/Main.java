package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task4.ValidatorXML;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];

		System.out.println("Input ==> " + xmlFileName);
		new ValidatorXML().check("input.xsd", xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowers = domController.parseXML();

		// sort (case 1)
		domController.sort(flowers);
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.createXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		flowers = saxController.parseXML();
		
		// sort  (case 2)
		saxController.sort(flowers);
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.createXML(flowers, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		flowers = staxController.parseXML();
		
		// sort  (case 3)
		staxController.sort(flowers);
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.createXML(flowers, outputXmlFile);
	}

}
