package com.epam.rd.java.basic.task1;

public class Parameter {
    private String measure;
    private int value;

    public Parameter() {}

    public Parameter(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public int getValue() {
        return value;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
