package com.epam.rd.java.basic.task5;

import com.epam.rd.java.basic.task1.Flower;
import com.epam.rd.java.basic.task1.Flowers;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

public class SAXCreateXML {
    public static final String CDATA = "CDATA";
    public static final String EMPTY = "";
    public static final String MEASURE = "measure";
    private Flowers flowers;
    public void createXML(Flowers flowers, String outputXmlFile) throws TransformerConfigurationException, SAXException {
        this.flowers = flowers;
        SAXTransformerFactory tf = (SAXTransformerFactory) TransformerFactory.newInstance();
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, EMPTY);
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, EMPTY);
        TransformerHandler hd = tf.newTransformerHandler();
        Transformer serializer = hd.getTransformer();
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
        hd.setResult(new StreamResult(outputXmlFile));
        buildDocument(hd);
    }

    private void buildDocument(TransformerHandler hd) throws SAXException {
        AttributesImpl atts = new AttributesImpl();

        atts.addAttribute(EMPTY, EMPTY,"xmlns", CDATA,"http://www.nure.ua");
        atts.addAttribute(EMPTY, EMPTY, "xmlns:xsi",CDATA,
                "http://www.w3.org/2001/XMLSchema-instance");
        atts.addAttribute(EMPTY, EMPTY, "xsi:schemaLocation", CDATA,
                "http://www.nure.ua input.xsd");
        hd.startDocument();
        hd.startElement(EMPTY, EMPTY, "flowers",atts);
        for (Flower f : flowers.getList()) {
            buildChild(hd, atts, f);

        }
        hd.endElement(EMPTY, EMPTY, "flowers");
        hd.endDocument();
    }

    private void buildChild(TransformerHandler hd, AttributesImpl atts, Flower f) throws SAXException {
        atts.clear();
        hd.startElement(EMPTY, EMPTY,"flower" , atts);
        buildElement(hd, atts,"name", f.getName());
        buildElement(hd, atts, "soil", f.getSoil().getName());
        buildElement(hd, atts,"origin", f.getOrigin());
        buildVisualParameters(hd, atts, f);
        builGrowingTips(hd, atts, f);
        buildElement(hd, atts,"multiplying", f.getMultiplying().getName());
        hd.endElement(EMPTY, EMPTY, "flower");
    }

    private void buildElement(TransformerHandler hd, AttributesImpl atts, String tag,
                              String value) throws SAXException {
        hd.startElement(EMPTY, EMPTY,tag , atts);
        hd.characters(value.toCharArray(),0,value.length());
        hd.endElement(EMPTY, EMPTY, tag);
    }

    private void buildVisualParameters(TransformerHandler hd, AttributesImpl atts, Flower f) throws SAXException {
        hd.startElement(EMPTY, EMPTY,"visualParameters" , atts);
        buildElement(hd, atts, "stemColour", f.getVp().getStemColour());
        buildElement(hd, atts, "leafColour", f.getVp().getLeafColour());
        atts.addAttribute(EMPTY, EMPTY, MEASURE, CDATA,
                f.getVp().getAveLenFlower().getMeasure());
        buildElement(hd, atts, "aveLenFlower", EMPTY + f.getVp().getAveLenFlower().getValue());
        atts.clear();
        hd.endElement(EMPTY, EMPTY, "visualParameters");
    }

    private void builGrowingTips(TransformerHandler hd, AttributesImpl atts, Flower f) throws SAXException {
        hd.startElement(EMPTY, EMPTY,"growingTips" , atts);
        atts.addAttribute(EMPTY, EMPTY, MEASURE, CDATA,
                f.getGt().getTempreture().getMeasure());
        buildElement(hd, atts,"tempreture", EMPTY + f.getGt().getTempreture().getValue());
        atts.clear();
        atts.addAttribute(EMPTY, EMPTY, "lightRequiring", CDATA,
                f.getGt().isLightRequiring() ? "yes" : "no");
        hd.startElement(EMPTY, EMPTY, "lighting", atts);
        hd.endElement(EMPTY, EMPTY, "lighting");
        atts.clear();
        atts.addAttribute(EMPTY, EMPTY, MEASURE, CDATA,
                f.getGt().getWatering().getMeasure());
        buildElement(hd, atts,"watering", EMPTY + f.getGt().getWatering().getValue());
        atts.clear();
        hd.endElement(EMPTY, EMPTY, "growingTips");
    }
}
