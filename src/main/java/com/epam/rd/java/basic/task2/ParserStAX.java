package com.epam.rd.java.basic.task2;

import com.epam.rd.java.basic.task1.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ParserStAX {
    public Flowers parseXML(String path) throws FileNotFoundException, XMLStreamException {
        Flowers flowers = new Flowers();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(path));
        Flower flower = new Flower();
        VisualParameters vp = new VisualParameters();
        GrowingTips gt = new GrowingTips();
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                parseElement(reader, event, flower,vp,gt);
            }
            if (event.isEndElement()){
                EndElement endElement = event.asEndElement();
                Tags tag = Tags.valueOf(endElement.getName().getLocalPart().toUpperCase());
                switch (tag){
                    case VISUALPARAMETERS:
                        flower.setVp(vp);
                        vp = new VisualParameters(); break;
                    case GROWINGTIPS:
                        flower.setGt(gt);
                        gt = new GrowingTips(); break;
                    case FLOWER:flowers.getList().add(flower);
                        flower = new Flower(); break;
                    default: break;
                }
            }
        }
        return flowers;
    }

    private void parseElement(XMLEventReader reader, XMLEvent event, Flower flower,
                              VisualParameters vp, GrowingTips gt) throws XMLStreamException {
        StartElement st = event.asStartElement();
        Tags tag = Tags.valueOf(st.getName().getLocalPart().toUpperCase());
        switch (tag) {
            case NAME:
                event = reader.nextEvent();
                flower.setName(event.asCharacters().getData());
                break;
            case SOIL:
                event = reader.nextEvent();
                flower.setSoil(event.asCharacters().getData());
                break;
            case ORIGIN:
                event = reader.nextEvent();
                flower.setOrigin(event.asCharacters().getData());
                break;
            case STEMCOLOUR: event = reader.nextEvent();
            vp.setStemColour(event.asCharacters().getData());
            break;
            case LEAFCOLOUR: event = reader.nextEvent();
                vp.setLeafColour(event.asCharacters().getData());
                break;
            case AVELENFLOWER:
                vp.setAveLenFlower(getParameter(reader, st));
                break;
            case TEMPRETURE: gt.setTempreture(getParameter(reader, st)); break;
            case LIGHTING: Attribute atte = st.getAttributeByName(new QName("lightRequiring"));
            if (atte != null) gt.setLightRequiring(atte.getValue().equals("yes"));
            break;
            case WATERING: gt.setWatering(getParameter(reader, st)); break;
            case MULTIPLYING: event = reader.nextEvent();
                flower.setMultiplying(event.asCharacters().getData());
                break;
            default: break;
        }
    }

    private Parameter getParameter(XMLEventReader reader, StartElement st) throws XMLStreamException {
        Parameter parameter = null;
        Attribute atte = st.getAttributeByName(new QName("measure"));
        if (atte != null){
            parameter = new Parameter();
            parameter.setMeasure(atte.getValue());
            XMLEvent event = reader.nextEvent();
            parameter.setValue(Integer.parseInt(event.asCharacters().getData()));
        }
        return parameter;
    }

}
