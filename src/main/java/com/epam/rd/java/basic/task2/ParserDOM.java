package com.epam.rd.java.basic.task2;

import com.epam.rd.java.basic.task1.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class ParserDOM {
    public Flowers parseXML(String path) throws ParserConfigurationException, IOException, SAXException {
        Flowers flowers = new Flowers();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(path);
        NodeList nl = document.getElementsByTagName("flower");
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if (n.hasChildNodes()){
                flowers.getList().add(parseNode(n.getChildNodes()));
            }
        }

        return flowers;
    }

    private Flower parseNode(NodeList childNodes) {
        return new Flower(
                childNodes.item(1).getTextContent(),
                childNodes.item(3).getTextContent(),
                childNodes.item(5).getTextContent(),
                getVisualParameter(childNodes.item(7).getChildNodes()),
                getGrowingTips(childNodes.item(9).getChildNodes()),
                childNodes.item(11).getTextContent()
        );
    }

    private GrowingTips getGrowingTips(NodeList childNodes) {
        return new GrowingTips(
                new Parameter(childNodes.item(1).getAttributes().getNamedItem("measure").getNodeValue(),
                        Integer.parseInt(childNodes.item(1).getTextContent())),
                childNodes.item(3).getAttributes().getNamedItem("lightRequiring").getNodeValue().equals("yes"),
                new Parameter(childNodes.item(5).getAttributes().getNamedItem("measure").getNodeValue(),
                        Integer.parseInt(childNodes.item(5).getTextContent()))
        );
    }

    private VisualParameters getVisualParameter(NodeList childNodes) {
        return new VisualParameters(
                childNodes.item(1).getTextContent(),
                childNodes.item(3).getTextContent(),
                new Parameter(childNodes.item(5).getAttributes().getNamedItem("measure").getNodeValue(),
                        Integer.parseInt(childNodes.item(5).getTextContent()))
        );
    }


}
