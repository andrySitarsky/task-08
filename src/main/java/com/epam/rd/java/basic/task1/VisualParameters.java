package com.epam.rd.java.basic.task1;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private Parameter aveLenFlower;

    public VisualParameters() {}

    public VisualParameters(String stemColour, String leafColour, Parameter aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public Parameter getAveLenFlower() {
        return aveLenFlower;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(Parameter aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
