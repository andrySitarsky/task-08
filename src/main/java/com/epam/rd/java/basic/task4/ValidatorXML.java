package com.epam.rd.java.basic.task4;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;

public class ValidatorXML {
    public void check(String pathXSD, String pathXML) throws SAXException {
        StreamSource fileXSD = new StreamSource(pathXSD);
        StreamSource fileXML = new StreamSource(pathXML);
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        try {
            Schema schema = factory.newSchema(fileXSD);
            Validator validator = schema.newValidator();
            validator.validate(fileXML);
            System.out.println(fileXML.getSystemId() + " is valid");
        } catch (SAXException e) {
            throw new SAXException(fileXML.getSystemId() + " is NOT valid reason:", e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
