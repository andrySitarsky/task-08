package com.epam.rd.java.basic.task3;

import com.epam.rd.java.basic.task1.Flower;
import com.epam.rd.java.basic.task1.Flowers;

import java.util.Comparator;

public class ContainerSorting {
    public void simpleSorting(Flowers flowers){
        flowers.getList().sort(Comparator.comparing(Flower::getName));
    }
    public void complexSorting(Flowers flowers){
        flowers.getList().sort(Comparator.comparing(Flower::getSoil)
                .thenComparing(obg -> obg.getVp().getAveLenFlower().getValue()));
    }
    public void reverseSorting(Flowers flowers){
        flowers.getList().sort(Comparator.comparing(obg -> obg.getMultiplying().getName(),
                Comparator.reverseOrder()));
    }
}
