package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task2.ParserSAX;
import com.epam.rd.java.basic.task3.ContainerSorting;
import com.epam.rd.java.basic.task5.SAXCreateXML;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws ParserConfigurationException, IOException, SAXException {
		return new ParserSAX().parseXML(xmlFileName);}

	public void sort(Flowers flowers){new ContainerSorting().complexSorting(flowers);}

	public void createXML(Flowers flowers, String outputXmlFile) throws TransformerConfigurationException, SAXException {
		new SAXCreateXML().createXML(flowers, outputXmlFile);
	}

}