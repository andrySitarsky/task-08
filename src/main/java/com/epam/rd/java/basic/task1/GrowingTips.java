package com.epam.rd.java.basic.task1;

public class GrowingTips {
    private Parameter tempreture;
    private boolean isLightRequiring;
    private Parameter watering;

    public GrowingTips() {}

    public GrowingTips(Parameter tempreture, boolean isLightRequiring, Parameter watering) {
        this.tempreture = tempreture;
        this.isLightRequiring = isLightRequiring;
        this.watering = watering;
    }

    public Parameter getTempreture() {
        return tempreture;
    }

    public boolean isLightRequiring() {
        return isLightRequiring;
    }

    public Parameter getWatering() {
        return watering;
    }

    public void setTempreture(Parameter tempreture) {
        this.tempreture = tempreture;
    }

    public void setLightRequiring(boolean lightRequiring) {
        isLightRequiring = lightRequiring;
    }

    public void setWatering(Parameter watering) {
        this.watering = watering;
    }
}
