package com.epam.rd.java.basic.task1;

public enum Multiplying {LEAVES("листья"),CUTTINGS("черенки"),SEEDS("семена");
    private String name;
    Multiplying(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
