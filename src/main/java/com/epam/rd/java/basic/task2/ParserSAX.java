package com.epam.rd.java.basic.task2;

import com.epam.rd.java.basic.task1.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class ParserSAX {
    private Flowers flowers = new Flowers();

    public Flowers parseXML(String path) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setNamespaceAware(true);
        SAXParser parser = factory.newSAXParser();
        XMLHandler handler = new XMLHandler();
        parser.parse(path, handler);
        return flowers;
    }

    private class XMLHandler extends DefaultHandler {
        Tags tag;
        Flower flower;
        VisualParameters vp;
        GrowingTips gt;
        Parameter parameter;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            tag = Tags.valueOf(localName.toUpperCase());
            switch (tag) {
                case FLOWER:
                    flower = new Flower();
                    break;
                case VISUALPARAMETERS:
                    vp = new VisualParameters();
                    break;
                case GROWINGTIPS:
                    gt = new GrowingTips();
                    break;
                case AVELENFLOWER:
                case TEMPRETURE:
                case WATERING:
                    parameter = new Parameter();
                    parameter.setMeasure(attributes.getValue("measure"));
                    break;
                case LIGHTING:
                    gt.setLightRequiring(attributes.getValue("lightRequiring").equals("yes"));
                    break;
                default: break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            tag = Tags.valueOf(localName.toUpperCase());
            switch (tag) {
                case AVELENFLOWER:
                    vp.setAveLenFlower(parameter);
                    break;
                case TEMPRETURE:
                    gt.setTempreture(parameter);
                    break;
                case WATERING:
                    gt.setWatering(parameter);
                    break;
                case VISUALPARAMETERS:
                    flower.setVp(vp); break;
                case GROWINGTIPS:
                    flower.setGt(gt); break;
                case FLOWER:
                    flowers.getList().add(flower); break;
                default: break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            String s = new String(ch, start, length).trim();
            if (!s.isBlank()) {
                switch (tag) {
                    case NAME:
                        flower.setName(s);
                        break;
                    case SOIL:
                        flower.setSoil(s);
                        break;
                    case ORIGIN:
                        flower.setOrigin(s);
                        break;
                    case STEMCOLOUR:
                        vp.setStemColour(s);
                        break;
                    case LEAFCOLOUR:
                        vp.setLeafColour(s);
                        break;
                    case AVELENFLOWER:
                    case TEMPRETURE:
                    case WATERING:
                        parameter.setValue(Integer.parseInt(s));
                        break;
                    case MULTIPLYING:
                        flower.setMultiplying(s);
                        break;
                    default: break;
                }
            }
        }
    }
}
