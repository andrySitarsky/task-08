package com.epam.rd.java.basic.task5;

import com.epam.rd.java.basic.task1.Flower;
import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task1.Parameter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StAXCreateXML {

    public static final String TAB = "\n    ";
    public static final String DOUBLE_TAB = "\n        ";
    public static final String THIRD_TAB = "\n            ";
    public static final String TRANSITION = "\n";

    public void createXML(Flowers flowers, String outputXmlFile) throws IOException, XMLStreamException {
        XMLOutputFactory xof =  XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = xof.createXMLStreamWriter(new FileWriter(outputXmlFile));
        xsw.writeStartDocument();
        xsw.writeCharacters(TRANSITION);
        xsw.writeStartElement("flowers");
        xsw.writeAttribute("xmlns","http://www.nure.ua");
        xsw.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xsw.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        for (Flower f : flowers.getList()) {
            buildChild(xsw, f);
        }
        xsw.writeCharacters(TRANSITION);
        xsw.writeEndElement();
        xsw.writeEndDocument();

        xsw.flush();
        xsw.close();
    }

    private void buildChild(XMLStreamWriter xsw, Flower f) throws XMLStreamException {
        xsw.writeCharacters(TAB);
        xsw.writeStartElement("flower");
        buildSimpleElement(xsw,"name", f.getName(), DOUBLE_TAB);
        buildSimpleElement(xsw, "soil", f.getSoil().getName(), DOUBLE_TAB);
        buildSimpleElement(xsw, "origin", f.getOrigin(), DOUBLE_TAB);
        xsw.writeCharacters(DOUBLE_TAB);
        xsw.writeStartElement("visualParameters");
        buildSimpleElement(xsw, "stemColour", f.getVp().getStemColour(), THIRD_TAB);
        buildSimpleElement(xsw,"leafColour", f.getVp().getLeafColour(), THIRD_TAB);
        buildComplexElement(xsw, "aveLenFlower", f.getVp().getAveLenFlower(), THIRD_TAB);
        xsw.writeEndElement();
        xsw.writeCharacters(DOUBLE_TAB);
        xsw.writeStartElement("growingTips");
        buildComplexElement(xsw, "tempreture", f.getGt().getTempreture(), THIRD_TAB);
        xsw.writeCharacters(THIRD_TAB);
        xsw.writeEmptyElement("lighting");
        xsw.writeAttribute("lightRequiring", f.getGt().isLightRequiring() ? "yes" : "no");
        buildComplexElement(xsw, "watering", f.getGt().getWatering(), THIRD_TAB);
        xsw.writeEndElement();
        buildSimpleElement(xsw, "multiplying", f.getMultiplying().getName(), DOUBLE_TAB);
        xsw.writeCharacters(TAB);
        xsw.writeEndElement();
    }

    private void buildComplexElement(XMLStreamWriter xsw, String tag, Parameter parameter, String separator)
            throws XMLStreamException {
        xsw.writeCharacters(separator);
        xsw.writeStartElement(tag);
        xsw.writeAttribute("measure", parameter.getMeasure());
        xsw.writeCharacters("" + parameter.getValue());
        xsw.writeEndElement();
    }

    private void buildSimpleElement(XMLStreamWriter xsw, String tag, String value, String separator) throws XMLStreamException {
        xsw.writeCharacters(separator);
        xsw.writeStartElement(tag);
        xsw.writeCharacters(value);
        xsw.writeEndElement();
    }
}
