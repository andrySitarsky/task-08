package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task2.ParserStAX;
import com.epam.rd.java.basic.task3.ContainerSorting;
import com.epam.rd.java.basic.task5.StAXCreateXML;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws XMLStreamException, FileNotFoundException {
		return new ParserStAX().parseXML(xmlFileName);}

	public void sort(Flowers flowers){new ContainerSorting().reverseSorting(flowers);}

	public void createXML(Flowers flowers, String outputXmlFile) throws XMLStreamException, IOException {
		new StAXCreateXML().createXML(flowers, outputXmlFile);
	}

}