package com.epam.rd.java.basic.task1;

public enum Soil {PODZOLIC("подзолистая"),UNPAVED("грунтовая"),
    SOD_PODZOLIC("дерново-подзолистая");

    private String name;
    Soil(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
