package com.epam.rd.java.basic.task2;

public enum Tags {FLOWERS("flowers"), FLOWER("flower"), NAME("name"), SOIL("soil"), ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"), STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"), AVELENFLOWER("aveLenFlower"), GROWINGTIPS("growingTips"),
    TEMPRETURE("tempreture"), LIGHTING("lighting"), WATERING("watering"),
    MULTIPLYING("multiplying");
    private String name;

    Tags(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
