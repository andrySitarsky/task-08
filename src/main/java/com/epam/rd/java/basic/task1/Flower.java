package com.epam.rd.java.basic.task1;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;
    private VisualParameters vp;
    private GrowingTips gt;
    private Multiplying multiplying;

    public Flower() {}

    public Flower(String name, String soil, String origin, VisualParameters vp,
                  GrowingTips gt, String multiplying) {
        this.name = name;
        this.soil = findSoil(soil);
        this.origin = origin;
        this.vp = vp;
        this.gt = gt;
        this.multiplying = findMultiplying(multiplying);
    }

    private Multiplying findMultiplying(String multiplying) {
        for (Multiplying m : Multiplying.values()) {
            if (m.getName().equals(multiplying)) return m;
        }
        return null;
    }

    private Soil findSoil(String soil) {
        for (Soil s : Soil.values()) {
            if (s.getName().equals(soil)) return s;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public Soil getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getVp() {
        return vp;
    }

    public GrowingTips getGt() {
        return gt;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = findSoil(soil);
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVp(VisualParameters vp) {
        this.vp = vp;
    }

    public void setGt(GrowingTips gt) {
        this.gt = gt;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = findMultiplying(multiplying);
    }
}
