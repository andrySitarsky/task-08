package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task1.Flowers;
import com.epam.rd.java.basic.task2.ParserDOM;
import com.epam.rd.java.basic.task3.ContainerSorting;
import com.epam.rd.java.basic.task5.DOMCreateXML;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws ParserConfigurationException, IOException, SAXException {
		return new ParserDOM().parseXML(xmlFileName);}

	public void sort(Flowers flowers){new ContainerSorting().simpleSorting(flowers);}

	public void createXML(Flowers flowers, String outputXmlFile) throws ParserConfigurationException,
			IOException,TransformerException {
		new DOMCreateXML().createXML(flowers, outputXmlFile);
	}
}
