<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <h2>Flowers</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th rowspan="3">Name</th>
                        <th rowspan="3">Soil</th>
                        <th rowspan="3">Origin</th>
                        <th colspan="4">Visual Parameters</th>
                        <th colspan="5">Growing Tips</th>
                        <th rowspan="3">Multiplying</th>
                    </tr>
                    <tr bgcolor="#9acd32">
                        <th rowspan="2">Stem Color</th>
                        <th rowspan="2">Leaf Color</th>
                        <th colspan="2">Ave Len Flower</th>
                        <th colspan="2">Temperature</th>
                        <th rowspan="2">Light Requiring</th>
                        <th colspan="2">Watering</th>
                    </tr>
                    <tr bgcolor="#9acd32">
                        <th>measure</th>
                        <th>value</th>
                        <th>measure</th>
                        <th>value</th>
                        <th>measure</th>
                        <th>value</th>
                    </tr>
                    <xsl:for-each select="flowers/flower">
                        <tr>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="soil"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/stemColour"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/leafColour"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/aveLenFlower/@measure"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/aveLenFlower"/>
                            </td>
                            <td>
                                <xsl:value-of select="growingTips/tempreture/@measure"/>
                            </td>
                            <td>
                                <xsl:value-of select="growingTips/tempreture"/>
                            </td>
                            <td>
                                <xsl:value-of select="growingTips/lighting/@lightRequiring"/>
                            </td>
                            <td>
                                <xsl:value-of select="growingTips/watering/@measure"/>
                            </td>
                            <td>
                                <xsl:value-of select="growingTips/watering"/>
                            </td>
                            <td>
                                <xsl:value-of select="multiplying"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>